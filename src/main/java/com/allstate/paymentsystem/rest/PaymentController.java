package com.allstate.paymentsystem.rest;

import com.allstate.paymentsystem.entities.Payment;
import com.allstate.paymentsystem.service.IPaymentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api")
public class PaymentController {
    private static final Logger LOGGER = LoggerFactory.getLogger(PaymentController.class);

    @Autowired
    IPaymentService service;

    @GetMapping("/status")
    public ResponseEntity<String> getStatus() {
        LOGGER.info("/api/status has been called");
        return new ResponseEntity<String>("Rest API is running", HttpStatus.OK);
    }

    @GetMapping("/count")
    public ResponseEntity<Integer> count() {
        int result = service.rowCount();
        if(result <= 0) {
            LOGGER.warn("/api/count returned no results");
            return new ResponseEntity<Integer>(HttpStatus.NOT_FOUND);
        } else {
            LOGGER.info("/api/count has returned results");
            return new ResponseEntity<Integer>(result, HttpStatus.OK);
        }
    }

    @PostMapping("/save")
    public ResponseEntity<Integer> saveByIdHandling404(@RequestBody Payment payment) {
        int response = service.save(payment);
        if(response <=0) {
            LOGGER.warn("/api/save failed to save payment");
            return new ResponseEntity<Integer>(HttpStatus.NOT_FOUND);
        } else {
            LOGGER.info("/api/save has saved the payment");
            return new ResponseEntity<Integer>(response, HttpStatus.OK);
        }
    }

    @GetMapping("/findById/{id}")
    public ResponseEntity<Payment> getPaymentByIdHandling404(@PathVariable("id") int id) {
        Payment payment = service.findById(id);
        if(payment == null) {
            LOGGER.warn("/api/findById returned no results");
            return new ResponseEntity<Payment>(HttpStatus.NOT_FOUND);
        } else {
            LOGGER.info("/api/findById has returned results");
            return new ResponseEntity<Payment>(payment, HttpStatus.OK);
        }
    }

    @GetMapping("/findByType/{type}")
    public ResponseEntity<Collection<Payment>> getPaymentByTypeHandling404(@PathVariable("type") String type) {
        Collection<Payment> payment = service.findByType(type);
        if(payment.size() == 0) {
            LOGGER.warn("/api/findByType returned no results");
            return new ResponseEntity<Collection<Payment>>(HttpStatus.NOT_FOUND);
        } else {
            LOGGER.info("/api/findByType has returned results");
            return new ResponseEntity<Collection<Payment>>(payment, HttpStatus.OK);
        }
    }

    @GetMapping("/findByCustId/{custId}")
    public ResponseEntity<Collection<Payment>> getPaymentByCustIdHandling404(@PathVariable("custId") int custId) {
        Collection<Payment> payment = service.findByCustId(custId);
        if(payment.size() == 0) {
            LOGGER.warn("/api/findByType returned no results");
            return new ResponseEntity<Collection<Payment>>(HttpStatus.NOT_FOUND);
        } else {
            LOGGER.info("/api/findByType has returned results");
            return new ResponseEntity<Collection<Payment>>(payment, HttpStatus.OK);
        }
    }
}
