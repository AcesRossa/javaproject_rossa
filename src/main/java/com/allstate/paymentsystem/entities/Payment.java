package com.allstate.paymentsystem.entities;

import com.allstate.paymentsystem.exceptions.PaymentException;
import org.bson.types.ObjectId;

import java.util.Date;

public class Payment {
    private int paymentId;
    private Date paymentDate;
    private String type;
    private double amount;
    private int custId;

    public Payment() {
    }

    public Payment(int paymentId, Date paymentDate, String type, double amount, int custId) {
        this.paymentId = paymentId;
        this.paymentDate = paymentDate;
        this.type = type;
        this.amount = amount;
        this.custId = custId;
    }

    public int getPaymentId() {
        return paymentId;
    }

    public void setId(int paymentId) {
        this.paymentId = paymentId;
    }

    public Date getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(Date paymentDate) {
        this.paymentDate = paymentDate;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        if(amount<0) {
            throw new PaymentException("Amount > 0");
        } else {
            this.amount = amount;
        }
    }

    public int getCustId() {
        return custId;
    }

    public void setCustId(int custId) {
        this.custId = custId;
    }

    @Override
    public String toString() {
        return "" + paymentId + " " + paymentDate + " " + type + " " + amount + " " + custId;
    }
}
