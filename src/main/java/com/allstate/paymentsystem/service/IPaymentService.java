package com.allstate.paymentsystem.service;

import com.allstate.paymentsystem.entities.Payment;

import java.util.List;

public interface IPaymentService {
    int rowCount();
    Payment findById(int id);
    List<Payment> findByCustId(int custId);
    List<Payment> findByType(String type);
    int save(Payment payment);
}
