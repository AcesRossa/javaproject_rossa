package com.allstate.paymentsystem.service;

import com.allstate.paymentsystem.dao.IPaymentData;
import com.allstate.paymentsystem.entities.Payment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PaymentService implements IPaymentService {
    @Autowired
    IPaymentData dao;

    @Override
    public int rowCount() {
        return dao.rowCount();
    }

    @Override
    public Payment findById(int id) {
        if(id > 0) return dao.findById(id);
        else return null;
    }

    @Override
    public List<Payment> findByType(String type) {
        if(type != null) return dao.findByType(type);
        else return null;
    }

    @Override
    public List<Payment> findByCustId(int custId) {
        if(custId > 0) return dao.findByCustId(custId);
        else return null;
    }

    @Override
    public int save(Payment payment) {
        return dao.save(payment);
    }
}
