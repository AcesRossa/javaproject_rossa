package com.allstate.paymentsystem.dao;

import com.allstate.paymentsystem.entities.Payment;

import java.util.List;

public interface IPaymentData {
    int rowCount();
    Payment findById(int id);
    List<Payment> findByCustId(int custId);
    List<Payment> findByType(String type);
    int save(Payment payment);
    void dropDb();
}
