package com.allstate.paymentsystem.dao;

import com.allstate.paymentsystem.entities.Payment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;

@Repository
public class PaymentRepoImpl implements IPaymentData {
    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public int rowCount() {
        Collection<Payment> payments = mongoTemplate.findAll(Payment.class);
        return payments.size();
    }

    @Override
    public Payment findById(int paymentId) {
        Query query = new Query();
        query.addCriteria(Criteria.where("paymentId").is(paymentId));
        return mongoTemplate.findOne(query, Payment.class);
    }

    @Override
    public List<Payment> findByType(String type) {
        Query query = new Query();
        query.addCriteria(Criteria.where("type").is(type));
        return mongoTemplate.find(query, Payment.class);
    }

    @Override
    public List<Payment> findByCustId(int custId) {
        Query query = new Query();
        query.addCriteria(Criteria.where("custId").is(custId));
        return mongoTemplate.find(query, Payment.class);
    }

    @Override
    public int save(Payment payment) {
        mongoTemplate.insert(payment);
        return payment.getCustId();
    }

    @Override
    public void dropDb() {
        for(String collectionName: mongoTemplate.getCollectionNames()) {
            if(!collectionName.startsWith("system.")) {
                mongoTemplate.dropCollection(collectionName);
            }
        }
    }
}
