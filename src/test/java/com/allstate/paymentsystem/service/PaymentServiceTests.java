package com.allstate.paymentsystem.service;

import com.allstate.paymentsystem.dao.IPaymentData;
import com.allstate.paymentsystem.entities.Payment;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Date;

@SpringBootTest
public class PaymentServiceTests {
    @Autowired
    PaymentService service;

    @Autowired
    IPaymentData dao;

    @BeforeEach
    public void cleanUp() { dao.dropDb(); }

    @Test
    public void addPaymentsThroughServiceLayerAndTestRowCount() {
        Date date = new Date();
        service.save(new Payment(1, date, "Car", 10, 100));
        Assert.assertEquals(1, service.rowCount());

    }

    @Test
    public void findByIdThroughServiceLayer() {
        Date date = new Date();
        service.save(new Payment(1, date, "Car", 10, 100));
        service.save(new Payment(2, date, "Mobile Phone", 20, 200));
        service.save(new Payment(3, date, "Mortgage", 30, 300));
        Assert.assertEquals(200, service.findById(2).getCustId());
    }

    @Test
    public void findByTypeThroughServiceLayer() {
        Date date = new Date();
        service.save(new Payment(1, date, "Car", 10, 100));
        service.save(new Payment(2, date, "Mobile Phone", 20, 200));
        service.save(new Payment(3, date, "Car", 30, 300));
        Assert.assertEquals(2, service.findByType("Car").size());
        Assert.assertEquals(300, service.findByType("Car").get(1).getCustId());
    }
}
