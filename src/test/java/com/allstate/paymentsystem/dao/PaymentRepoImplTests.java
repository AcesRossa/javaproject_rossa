package com.allstate.paymentsystem.dao;

import com.allstate.paymentsystem.entities.Payment;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;

import java.util.Date;
import java.util.List;

@SpringBootTest
public class PaymentRepoImplTests {
    @Autowired
    IPaymentData dao;

    @Autowired
    MongoTemplate mongoTemplate;

    @BeforeEach
    public void cleanUp() {
        dao.dropDb();
    }

    @Test
    public void testSave() {
        Date date = new Date();
        Payment payment1 = new Payment(1, date, "Car", 10, 100);
        Payment payment2 = new Payment(2, date, "Mobile Phone", 20, 200);
        Payment payment3 = new Payment(3, date, "Mortgage", 30, 300);

        dao.save(payment1);
        dao.save(payment2);
        dao.save(payment3);

        List<Payment> payments = mongoTemplate.findAll(Payment.class);
        Assert.assertEquals(200, payments.get(1).getCustId());
    }

    @Test
    public void testRowCount() {
        Date date = new Date();
        Payment payment1 = new Payment(1, date, "Car", 10, 100);
        Payment payment2 = new Payment(2, date, "Mobile Phone", 20, 200);
        Payment payment3 = new Payment(3, date, "Mortgage", 30, 300);

        dao.save(payment1);
        dao.save(payment2);
        dao.save(payment3);

        int rowCount = dao.rowCount();
        Assert.assertEquals(3, rowCount);
    }

    @Test
    public void testFindById() {
        Date date = new Date();
        Payment payment1 = new Payment(1, date, "Car", 10, 100);
        Payment payment2 = new Payment(2, date, "Mobile Phone", 20, 200);
        Payment payment3 = new Payment(3, date, "Mortgage", 30, 300);

        dao.save(payment1);
        dao.save(payment2);
        dao.save(payment3);

        Payment paymentResult = dao.findById(1);
        Assert.assertEquals("Car", paymentResult.getType());
        Assert.assertEquals(100, paymentResult.getCustId());
    }

    @Test
    public void testFindByType() {
        Date date = new Date();
        Payment payment1 = new Payment(1, date, "Car", 10, 100);
        Payment payment2 = new Payment(2, date, "Car", 20, 200);
        Payment payment3 = new Payment(3, date, "Mortgage", 30, 300);

        dao.save(payment1);
        dao.save(payment2);
        dao.save(payment3);

        List<Payment> paymentResult = dao.findByType("Car");
        Assert.assertEquals(2, paymentResult.size());
        Assert.assertEquals(100, paymentResult.get(0).getCustId());
        Assert.assertEquals(200, paymentResult.get(1).getCustId());
    }

    @Test
    public void testFindByCustId() {
        Date date = new Date();
        Payment payment1 = new Payment(1, date, "Car", 10, 100);
        Payment payment2 = new Payment(2, date, "Car", 20, 200);
        Payment payment3 = new Payment(3, date, "Mortgage", 30, 300);

        dao.save(payment1);
        dao.save(payment2);
        dao.save(payment3);

        List<Payment> paymentResult = dao.findByCustId(300);
        Assert.assertEquals(1, paymentResult.size());
        Assert.assertEquals(3, paymentResult.get(0).getPaymentId());
        Assert.assertEquals("Mortgage", paymentResult.get(0).getType());
        Assert.assertEquals(300, paymentResult.get(0).getCustId());
    }


}
