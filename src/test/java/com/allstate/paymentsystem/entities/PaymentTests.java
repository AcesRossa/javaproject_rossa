package com.allstate.paymentsystem.entities;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

import java.util.Date;

public class PaymentTests {
    @Test
    public void testConstructor() {
        Date date = new Date();
        Payment payment = new Payment(1, date, "Test", 20, 1);
        Assert.assertEquals("Payment Id", 1, payment.getPaymentId());
        Assert.assertEquals("Type", "Test", payment.getType());
        Assert.assertEquals("Amount", 20, payment.getAmount(), 0.01);
        Assert.assertEquals("CustId", 1, payment.getCustId());
    }
}
